# Using a Python Library to get an Access Token
Here we discuss how to use a Python library to generate an access token. This is
much simpler than [using the command line](auth-cli.md).

## Using the AuthLib Python Library
There are libraries for most programming languages that allow you to generate
an access token from the client information shown in
[Iris](https://iris.nersc.gov). In this example, we will use the
[Authlib](https://authlib.org/) Python library.

After installing the Authlib library via either `conda` or `pip`, we can then
use it to generate an access token from the key pair and client ID created in
Iris:

```python
from authlib.integrations.requests_client import OAuth2Session
from authlib.oauth2.rfc7523 import PrivateKeyJWT

token_url = "https://oidc.nersc.gov/c2id/token"
client_id = "<your client id>"
private_key = "<your private key>"

client = OAuth2Session(client_id=client_id,
                        client_secret=private_key, 
                        token_endpoint_auth_method="private_key_jwt")
client.register_client_auth_method(PrivateKeyJWT(token_url))
resp = client.fetch_token(token_url, grant_type="client_credentials")
token = resp["access_token"]
```

The final variable `token` is the access token you can use to access the SuperFacility API.