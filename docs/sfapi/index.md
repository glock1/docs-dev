# Superfacility API Documentation

This page presents some general information about the NERSC Superfacility 
Application Programming Interface (API). 

This API can be used to script jobs or workflows running against NERSC systems.
The goal is to support everything from a simple script that submits a job to
complex workflows that move files, check progress and make reservations.

## API endpoint and reference

The API endpoint can be accessed here: https://api.nersc.gov/api/v1.2/

That is also the place to find the up-to-date reference documentation, made
with [swagger.io](https://swagger.io/).

!!! danger "API endpoints and documentation subject to change"
    Due to the rapid evolution of the SF API, we will not host a separate
    reference until the API reaches a certain maturity. Do not consider any of
    the endpoints as stable, some endpoints are currently just placeholders.

## Authentication

Authentication to the Superfacility API is handled by submitting an
[OAuth](https://en.wikipedia.org/wiki/OAuth) access token (JWT) with your API call.

### Create a SuperFacility Client in Iris
In order to create an access token, you first need to create a
"Superfacility client" in [Iris](https://iris.nersc.gov). This can be done on
the [profile page](https://iris.nersc.gov/profile) in the
"Superfacility API Clients" section. When creating a client, you can specify if
there are any limitations on how you want to use the API. 

You only need to create the SuperFacility Client once. Until it is deleted you
can keep using its client-id and key-pair.

### Save the client information
Once you have filled out the new client form, you will receive a
private/public key-pair and a client ID. It is important to save the private
key in a secure location, as **you will not be able to view it again in Iris**.

### Exchange a "client assertion" for an "access token"
Next you need to use your private key to generate a client assertion that can
be exchanged for an access token. You have multiple ways to do this:

- The recommended way is to use a [software library](auth/auth-lib.md)
- alternatively, you can also use [standard tools on the command line](auth/auth-cli.md) to do the same thing.

### Call the SuperFacility API with the access token
Once you have the access token, you can call the sfapi:
```bash
curl -X POST "https://api.nersc.gov/api/v1.2/utilities/command/dtn01" -H "accept: application/json" -H "Authorization: $ACCESS_TOKEN" -H "Content-Type: application/x-www-form-urlencoded" -d "executable=hostname"
```

Or from python, using the [requests](https://requests.readthedocs.io/en/master/) library:
```python
import requests
access_token='<your access token goes here>'
r = requests.get("https://api.nersc.gov/api/v1.2/tasks?tags=%20", headers={ "accept": "application/json", "Authorization": access_token})
print(r.json())
```

Your access token is good for 5-10 mins before you'll need a new one.
