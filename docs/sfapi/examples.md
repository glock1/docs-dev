# SF API Examples

All of the following Python examples assume that an access token has been
created in the following manner:

```python
from authlib.integrations.requests_client import OAuth2Session
from authlib.oauth2.rfc7523 import PrivateKeyJWT

token_url = "https://oidc.nersc.gov/c2id/token"
client_id = "<your client id>"
private_key = "<your private key>"

client = OAuth2Session(client_id=client_id,
                        client_secret=private_key, 
                        token_endpoint_auth_method="private_key_jwt")
client.register_client_auth_method(PrivateKeyJWT(token_url))
resp = client.fetch_token(token_url, grant_type="client_credentials")
token = resp["access_token"]
```

## Checking System Status

!!! danger "This feature is not yet fully functional"
    The SF API is currently in active development; system status retrieval
    will be in place in the future.

To check the status of a given NERSC system, one can use the "status" method
and specify the name of the NERSC status of interest. For example, to get the
status of Cori:

```python
import requests

system = "cori"
r = requests.get("https://api.nersc.gov/api/v1.2/status/"+system,
                 headers={ "accept": "application/json", "Authorization": token})
print(r.json())
```


## Listing Directory Contents

To list the contents of a directory on a NERSC file system, the `ls` utility
method can be used in combination with the directory of interest. For example,
to list the contents of one's home directory on Cori, we specify both the
system and the full directory path to the request:

```python
import requests

system = "cori"
home = "<your home dir>" # (e.g., /global/homes/u/username)
r = requests.get("https://api.nersc.gov/api/v1.2/utilities/ls/"+system+home,
                 headers={ "accept": "application/json", "Authorization": token})
print(r.json())
```
