# Using the command line to get an Access Token
Here we discuss how to use the command line and some standard tools (curl, openssl, etc) to create a client assertion and exchange it for an access token. You may find it simpler to [use a library](auth-lib.md) instead.

## Save the client id to a json file
First, create a json file called `payload.json`:
```json
{
        "iss" : "<client_id>",
        "sub" : "<client_id>",
        "aud" : "https://oidc.nersc.gov/c2id/token",
        "exp" : <expiration timestamp>
}
```

"client_id" is your client id from Iris.

The expiration timestamp should be in the future. You can get a future date (eg. 5 mins from now) from the GNU date command:
```bash
date --date 'now + 5 minutes' "+%s"
```

or from BSD date:
```bash
date -v +5M "+%s"
```

## Create the client assertion using the "jose" tool
Next we need to create a client assertion (a JWT), by taking the `payload.json` file, adding a header and signing it with our private key. The simple way to do this is to install the [jose](https://formulae.brew.sh/formula/jose) tool, and run:

```bash
ASSERTION=$(jose jws sig -I payload.json -k my-keyset.jwk -c)
```

Here, my-keyset.jwk is private key in JWK format as shown by Iris.

## Or, create the client assertion using standard tools
Alternatively, you can create the client assertion using standard tools. [Iris](https://iris.nersc.gov) shows the generated client keys in both JWK and PEM formats. Using the latter, you can construct a JWT using openssl. Save the PEM-formatted private key as priv_key.pem and run:

```bash
echo -n '{ "alg": "RS256" }' | openssl base64 -A | tr '/+' '_-' | tr -d '=' > head.b64
openssl base64 -in payload.json -A | tr '/+' '_-' | tr -d '=' > body.b64
cat head.b64 <(echo '.') body.b64 | tr -d "\n" > jwt.txt
openssl dgst -sha256 -sign priv_key.pem jwt.txt | openssl base64 -A | tr '/+' '_-' | tr -d '=' > sig.sha256.b64
ASSERTION=`cat jwt.txt <(echo '.') sig.sha256.b64 | tr -d "\n"`
```

The `tr` command trailing the `openssl` lines translates [base64](https://en.wikipedia.org/wiki/Base64) to the [base64url](https://tools.ietf.org/html/rfc4648#section-5) encoding that JWTs use.

## Exchange the client assertion for an access token
Now you're ready to exchange the encrypted assertion for a short-lived access token:

```bash
curl -s -XPOST -H "Content-Type:application/x-www-form-urlencoded"  -d "grant_type=client_credentials&client_assertion_type=urn%3Aietf%3Aparams%3Aoauth%3Aclient-assertion-type%3Ajwt-bearer&client_assertion=$ASSERTION"  https://oidc.nersc.gov/c2id/token
```

The returned json will contain the access token under the "access_token" key.
