# This Dockerfile creates an image which downloads the latest version of
# docs-dev from the GitLab repository and renders them into HTML using `mkdocs
# build`.

FROM ubuntu:latest

WORKDIR /tmp

ENV TERM=xterm
ENV PATH=/opt/conda/bin:${PATH}
ENV DEBIAN_FRONTEND=noninteractive

RUN \
    apt-get update && \
    apt-get install -y apt-utils dialog git && \
    apt-get -y upgrade && \
    apt-get install -y python3 wget && \
    wget https://repo.continuum.io/miniconda/Miniconda3-latest-Linux-x86_64.sh -O miniconda.sh && \
    bash miniconda.sh -b -p /opt/conda && \
    rm /tmp/miniconda.sh && \
    conda config --set always_yes yes && \
    conda update -n base conda && \
    conda install -n base pip && \
    conda clean -a && \
    apt-get -y autoclean
ENTRYPOINT \
    git clone https://gitlab.com/NERSC/docs-dev.git && \
    cd docs-dev && \
    pip install --no-cache -r requirements.txt && \
    mkdocs build -d /mkdocs-build
